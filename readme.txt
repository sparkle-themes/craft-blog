=== Craft Blog ===
Contributors: sparklewpthemes
Tags: blog, news, one-column, two-columns, left-sidebar, right-sidebar, custom-background, custom-colors, custom-header, custom-logo, custom-menu, editor-style, post-formats, sticky-post, theme-options, translation-ready, featured-images
Requires at least: 4.7
Tested up to: 6.2
Requires PHP: 5.6
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Craft Blog is a clean, sleek modern, stylish and beautiful free WordPress blog theme, Craft Blog perfect for lifestyle & travel bloggers, style guides, personal blogging, journal, music band & singers, photographers, writers, fashion designer, interior designers, wedding, eCommerce and more. 

== Description ==

Craft Blog is a clean, sleek modern, stylish and beautiful free WordPress blog theme, Craft Blog perfect for lifestyle & travel bloggers, style guides, personal blogging, journal, music band & singers, photographers, writers, fashion designer, interior designers, wedding, eCommerce and more. Craft Blog is a modern theme with elegant design and is completely built on Customizer which allows you to customize theme settings easily with live previews. Craft Blog fully responsive, cross-browser compatible, translation ready and search engine optimized ( SEO ) friendly clean and modern design theme.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Translation ==

Craft Blog theme is translation ready.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Theme supports Jetpack, Contact Form 7, AccessPress Social Share, AccessPress Social Counter many more.

= Where can I find theme all features ? =

You can check our Theme features at http://demo.sparklewpthemes.com/craftblog/.

= Where can I find theme demo? =

You can check our Theme Demo at http://sparklewpthemes.com/wordpress-themes/craftblog/

== Copyright ==

Craft Blog WordPress Theme, Copyright (C) 2018 Sparkle Themes.
Craft Blog is distributed under the terms of the GNU GPL


== Credits ==

* Based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)

* The exceptions to license are as follows:

* respond, Created by Copyright 2011 Scott Jehl, scottjehl.com ( https://github.com/scottjehl/Respond ), MIT License: http://opensource.org/licenses/MIT

* html5shiv, Created by @afarkas @jdalton @jon_neal @rem ( https://github.com/afarkas/html5shiv ), MIT License: http://opensource.org/licenses/MIT

* Google Fonts - Apache License, version 2.0

* Font-Awesome https://github.com/FortAwesome/Font-Awesome [MIT](http://opensource.org/licenses/MIT)- by @davegandy, The Font Awesome font is licensed under the SIL OFL 1.1: http://scripts.sil.org/OFL

* Theia Sticky Sidebar v1.6.0, https://github.com/WeCodePixels/theia-sticky-sidebar, Copyright 2013-2016 WeCodePixels and other contributors, Released under the MIT license (http://www.opensource.org/licenses/mit-license.php)

* Owl Carousel v2.3.4, Copyright 2013-2018 David Deutsch, Licensed under: SEE LICENSE IN https://github.com/OwlCarousel2/OwlCarousel2/blob/master/LICENSE

* Bootstrap v3.3.7 (http://getbootstrap.com), Copyright 2011-2016 Twitter, Inc., licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)

* Lightslider - v1.1.6 - 2016-10-25, https://github.com/sachinchoolur/lightslider, Copyright (c) 2016 Sachin N; Licensed MIT */

* Sticky Plugin , Author: Anthony Garand, Improvements by German M. Bravo (Kronuz) and Ruud Kamphuis (ruudk), Improvements by Leonardo C. Daronco (daronco), Created: 2/14/2011, Date: 16/04/2015, Website: http://labs.anthonygarand.com/sticky

* BreadcrumbTrail, Justin Tadlock <justin@justintadlock.com>, Copyright (c) 2008 - 2017, Justin Tadlock, license  under the GPL license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html


== Resources ==

	All the images are used from http://pxhere.com under License CC0 Public Domain or self taken are fully GPL compatible.

	https://pxhere.com/en/photo/1622685  © CC0 Public Domain


== Changelog ==
= 1.0.7 13th April 2023 =
** WordPress 6.2 compatible
** FSE Editor Compatible

= 1.0.6 13th Nov 2022 =
** WordPress 6.1 compatible
** Minor Changes on Design
** Tested WordPress 5.7.2

= 1.0.5 8th June 2021 =
** Customizer Css issue Fixed
** Selective Refesh added for social media ans slider
** Tested WordPress 5.7.2

= 1.0.3 6th June 2020 =
** Customizer Css issue Fixed
** Selective Refesh added for social media ans slider
** Tested WordPress 5.4.1


= 1.0.2 11th February 2019 =

** Add 3 custom widget.
** Update .pot file.
** Fixed breadcrumbs tags issue.

= 1.0.1 10th january 2019 =

** Add the Upgrade PRO version button link and information.
** Add Post Feed Layout options.
** Add web page layout (boxed or full width).
** Add one click import demo data.
** Add about theme information page.
** Fixed responsive design issue.
** Update .pot file.
** Add Footer widget area and mange in front.
** Fixed slider responsive issue.
** Add options to enable or diable sticky header menu.


= 1.0.0 4th December 2018 =

** Submitted theme for review in http://wordpress.org
