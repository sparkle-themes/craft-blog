<div class="welcome-upgrade-wrap">
    <div class="welcome-upgrade-header">
        <h3><?php printf(esc_html__('Premium Version of %s', 'craft-blog'), $this->theme_name); ?></h3>
        <p><?php echo sprintf(esc_html__('Check out the demos that you can create with the premium version of the %s theme. 10+ Pre-defined demos can be imported with just one click in the premium version.', 'craft-blog'), $this->theme_name); ?></p>
    </div>

    <div class="recomended-plugin-wrap">
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/craftblogpro/demos/wp-content/uploads/sites/14/2020/12/1-12.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Craft-blog Pro - Main','craft-blog'); ?></span>
                <span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/craftblogpro/" class="button button-primary"><?php echo esc_html__('Preview', 'craft-blog'); ?></a>
				</span>
            </div>
        </div>

        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/craftblogpro/demos/wp-content/uploads/sites/14/2020/12/1-2.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Craft-blog Pro - Lifestyle','craft-blog'); ?></span>
				<span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/craftblogpro/lifestyle/" class="button button-primary"><?php echo esc_html__('Preview', 'craft-blog'); ?></a>
				</span>
            </div>
        </div>
        
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/craftblogpro/demos/wp-content/uploads/sites/14/2020/12/1-4.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Craft-blog Pro - Technology','craft-blog'); ?></span>
				<span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/craftblogpro/technology/" class="button button-primary"><?php echo esc_html__('Preview', 'craft-blog'); ?></a>
				</span>
            </div>
        </div>
        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/craftblogpro/demos/wp-content/uploads/sites/14/2020/12/1-7.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Craft-blog Pro - V4','craft-blog'); ?></span>
				<span class="plugin-btn-wrapper">
					<a target="_blank" href="https://demo.sparklewpthemes.com/craftblogpro/sample-v4/" class="button button-primary"><?php echo esc_html__('Preview', 'craft-blog'); ?></a>
				</span>
            </div>
        </div>

        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/craftblogpro/demos/wp-content/uploads/sites/14/2020/12/1-8.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Craft-blog Pro - V5','craft-blog'); ?></span>
                <span class="plugin-btn-wrapper">
                    <a target="_blank" href="https://demo.sparklewpthemes.com/craftblogpro/sample-v5/" class="button button-primary"><?php echo esc_html__('Preview', 'craft-blog'); ?></a>
                </span>
            </div>
        </div>

        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/craftblogpro/demos/wp-content/uploads/sites/14/2020/12/1-9.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Craft-blog Pro - V6','craft-blog'); ?></span>
                <span class="plugin-btn-wrapper">
                    <a target="_blank" href="https://demo.sparklewpthemes.com/craftblogpro/sample-v6/" class="button button-primary"><?php echo esc_html__('Preview', 'craft-blog'); ?></a>
                </span>
            </div>
        </div>

        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/craftblogpro/demos/wp-content/uploads/sites/14/2020/12/1-10.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Craft-blog Pro - V7','craft-blog'); ?></span>
                <span class="plugin-btn-wrapper">
                    <a target="_blank" href="https://demo.sparklewpthemes.com/craftblogpro/sample-v7/" class="button button-primary"><?php echo esc_html__('Preview', 'craft-blog'); ?></a>
                </span>
            </div>
        </div>

        <div class="recommended-plugins">
            <div class="plugin-image">
                <img src="https://demo.sparklewpthemes.com/craftblogpro/demos/wp-content/uploads/sites/14/2020/12/1-11.png">
            </div>

            <div class="plugin-title-wrap">
                <span class="title"><?php esc_html_e('Craft-blog Pro - V8','craft-blog'); ?></span>
                <span class="plugin-btn-wrapper">
                    <a target="_blank" href="https://demo.sparklewpthemes.com/craftblogpro/sample-v8/" class="button button-primary"><?php echo esc_html__('Preview', 'craft-blog'); ?></a>
                </span>
            </div>
        </div>
        
    </div>
</div>

<div class="welcome-upgrade-box">
    <div class="upgrade-box-text">
        <h3><?php echo esc_html__('Upgrade To Premium Version (14 Days Money Back Guarantee)', 'craft-blog'); ?></h3>
        <p><?php echo sprintf(esc_html__('With %s Pro theme you can create a beautiful website. If you want to unlock more possibilities then upgrade to the premium version, try the Premium version and check if it fits your need or not. If not, we have 14 days money-back guarantee.', 'craft-blog'), $this->theme_name); ?></p>
    </div>

    <a class="upgrade-button" href="https://sparklewpthemes.com/wordpress-themes/craftblogpro/" target="_blank"><?php esc_html_e('Upgrade Now', 'craft-blog'); ?></a>
</div>

