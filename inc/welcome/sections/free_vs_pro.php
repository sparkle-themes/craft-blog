<div class="free-vs-pro-info-wrap">
    <div class="free-vs-pro-info">
        <h4><?php esc_html_e('ONE TIME PAYMENT', 'craft-blog'); ?></h4>
        <p><?php esc_html_e('No renewal needed', 'craft-blog'); ?></p>
    </div>

    <div class="free-vs-pro-info">
        <h4><?php esc_html_e('UNLIMITED DOMAIN LICENCE', 'craft-blog'); ?></h4>
        <p><?php esc_html_e('Use in as many websites as you need', 'craft-blog'); ?></p>
    </div>

    <div class="free-vs-pro-info">
        <h4><?php esc_html_e('FREE UPDATES FOR LIFETIME', 'craft-blog'); ?></h4>
        <p><?php esc_html_e('Keep up to date', 'craft-blog'); ?></p>
    </div>
</div>

<table class="free-vs-pro form-table comparision-table">
	<thead>
		<tr>
			<th>
				<a href="<?php echo esc_url('https://sparklewpthemes.com/wordpress-themes/craftblogpro/'); ?>" target="_blank" class="button button-primary button-hero">
					<?php esc_html_e( 'Get Craft Blog Pro', 'craft-blog' ); ?>
				</a>
			</th>
			<th class="compare-icon"><?php esc_html_e( 'Craft Blog', 'craft-blog' ); ?></th>
			<th class="compare-icon"><?php esc_html_e( 'Craft Blog Pro', 'craft-blog' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<h3><?php esc_html_e( '800+ Google Fonts', 'craft-blog' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Header Background Image/Color/Video', 'craft-blog' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Unlimited Colors Options', 'craft-blog' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Classic, List, Grid Post Layouts', 'craft-blog' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Advanced Slider Options', 'craft-blog' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'WooCommerce Support', 'craft-blog' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Promo Section', 'craft-blog' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Custom Widget', 'craft-blog' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Pre Loader', 'craft-blog' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-no"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Sticky Navigation', 'craft-blog' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Sticky Sidebar', 'craft-blog' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>
		<tr>
			<td>
				<h3><?php esc_html_e( 'Premium Support 24/7', 'craft-blog' ); ?></h3>
			</td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
			<td class="compare-icon"><span class="dashicons-before dashicons-yes"></span></td>
		</tr>

		<tr>
			<td colspan="3">
				<a href="<?php echo esc_url('https://sparklewpthemes.com/wordpress-themes/craftblogpro/'); ?>" target="_blank" class="button button-primary button-hero">
					<strong><?php esc_html_e( 'View Full Feature List', 'craft-blog' ); ?></strong>
				</a>
			</td>
		</tr>
	</tbody>
</table>
